// Екранування символів — заміна тексту керуючих символів на відповідні текстові установки

function createNewUser(){
    let firstName = prompt('Put your name');
    let lastName = prompt('Put your Surname');
    let birthday = prompt('Put your birthday. dd.mm.yyyy');
    let currentDate = new Date();

    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0]+this.lastName).toLowerCase();
        },
        getAge() {
            birthday = birthday.split('.');
            birthday = new Date(birthday[2], birthday[1] - 1, birthday[0]);
            return currentDate.getFullYear() - birthday.getFullYear();
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.getFullYear());
        },
    }
    return newUser
}
const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
